import { createContext, useContext, useReducer } from "react";

export type Timer = {
  name: string;
  duration: number;
};

type TimersState = {
  isRunning: boolean;
  timers: Timer[];
};

const initialState: TimersState = {
  isRunning: false,
  timers: [],
};

type TimersContextValue = TimersState & {
  startTimer: () => void;
  stopTimer: () => void;
  addTimer: (timerData: Timer) => void;
};

type TimersContextProviderProps = {
  children: React.ReactNode;
};

export const TimersContext = createContext<TimersContextValue | null>(null)

export function useTimersContext() {
  const ctx = useContext(TimersContext);

  if (!ctx) {
    throw new Error('Timer context is null (???)');
  }

  return ctx;
}

type StartTimerAction = {
  type: 'START_TIMER';
};

type StopTimerAction = {
  type: 'STOP_TIMER';
};

type AddTimerAction = {
  type: 'ADD_TIMER';
  payload: Timer;
};

type Action = StopTimerAction | StartTimerAction | AddTimerAction;

function timersReducer(state: TimersState, action: Action): TimersState {
  switch (action.type) {
    case 'START_TIMER':
      return {
        ...state,
        isRunning: true,
      };
    case 'STOP_TIMER':
      return {
        ...state,
        isRunning: false,
      };
    case 'ADD_TIMER':
      return {
        ...state,
        timers: [...state.timers, {
          name: action.payload.name,
          duration: action.payload.duration,
        }],
      };
    default:
      return state;
  }
}

export default function TimersContextProvider({children}: TimersContextProviderProps) {
  const [timersState, dispatch] = useReducer(timersReducer, initialState);

  const ctx = {
    isRunning: timersState.isRunning,
    timers: timersState.timers,
    startTimer: () => {
      dispatch({ type: 'START_TIMER' });
    },
    stopTimer: () => {
      dispatch({ type: 'STOP_TIMER' });
    },
    addTimer: (timerData: Timer) => {
      dispatch({ type: 'ADD_TIMER', payload: timerData });
    },
  };

  return (
    <TimersContext.Provider value={ctx}>
      {children}
    </TimersContext.Provider>
  );
}